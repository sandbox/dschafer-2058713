
-- PROJECT INFO --

Ubercart Order Bulk Statusupdates
A module for Ubercart 2.x on Drupal 7.x

Converted to D7 by 'Backoffice Thinking'

Written by 

Sjoerd Adema < sjoerd [at] synetic [dot] nl >

Synetic B.V. the Netherlands
http://www.synetic.nl

-- SUMMARY --

This project adds an extra order overview to the menu from which you can select multiple orders and change the status in batch. You can select if you also want to call the ubercart hooks for statusupdates, these will send e-mails with the update when configured. 

-- INSTALLATION AND CONFIGURATION --

- Place the module in the appropriate modules folder (somewhere under sites/all/modules/)
- Enable the module
- If you are not the primary user, enable the permission 'bulk order status update' for the roles that need access
- After flushing the menu cache a new page is available under 'admin/store/orders/bulk_statusupdate'

